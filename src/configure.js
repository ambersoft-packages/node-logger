const {getStackTrace} = require("./helpers");

const configure = (loggers) => {
  const logRef = console.log
  const errorRef = console.error
  const infoRef = console.info

  process.on('uncaughtException', function (error) {
    console.error(Date.now(), 'Caught exception: ' + error, error.stack.toString());

    for(const logger of loggers) {
      logger.error(error.toString(), error.stack.toString())
    }
  });

  process.on('unhandledRejection', function (error) {
    console.error(Date.now(), 'Caught exception: ' + error, error.stack.toString());

    for(const logger of loggers) {
      logger.error(error.toString(), error.stack.toString())
    }
  });

  // To capture all logs/errors/info
  console.log = (...args) => {
    logRef.apply(console, args);

    for(const logger of loggers) {
      logger.log(args.join(' '), getStackTrace())
    }
  }

  console.error = (...args) => {
    errorRef.apply(console, args);

    for(const logger of loggers) {
      logger.error(args.join(' '), getStackTrace())
    }
  }

  console.info = (...args) => {
    infoRef.apply(console, args);
    args.unshift('[INFO]')

    for(const logger of loggers) {
      logger.log(args.join(' '), getStackTrace())
    }
  }
}

module.exports = configure

