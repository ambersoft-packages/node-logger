const graylog2 = require("graylog2");

const logger = (host, port, source, facility, bufferSize = 262144) => new graylog2.graylog({
  servers: [
    {host, port},
  ],
  hostname: source, // the name of this host
  facility,     // the facility for these log messages
  bufferSize          // max UDP packet size, should never exceed the
});

module.exports = logger
