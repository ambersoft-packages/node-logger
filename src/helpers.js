const getStackTrace = () => {
  const obj = {};
  Error.captureStackTrace(obj, getStackTrace);
  return obj.stack;
}

module.exports = {
  getStackTrace
}